#include "m.h"
#include <iostream>
#include "Eigen/Dense"

namespace m {

	std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B) {
		std::vector< std::vector<int> > C(A.size(), std::vector<int>(B.size(), 0));

		int sum = 0;

		for (int i = 0; i<(int)A.size(); i++) {
			for (int j = 0; j<(int)B[0].size(); j++) {
				sum = 0;
				for (int k = 0; k<(int)A[0].size(); k++) {
					sum = sum + A[i][k] * B[k][j];
				}
				C[i][j] = sum;
			}
		}
		return C;
	}

}